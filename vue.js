var API = '65ec56e12aa79e26a209e9a0953f0871';
var URL = 'http://api.ipstack.com/check?access_key=' + API + '&fields=zip';
var weatherAPI = '&APPID=da9a8475510169e74ba915d135ba3124';

let weather = new Vue({
    el: '#weather',

    data: {
        time: "loading...",
        temperature: "temp",
        conditions: "cond",
        humidity: "hum",
        pressure: "pre",
    },
})

let forecast = new Vue({
    el: '#forecast',

    data: {
        date: ["","","","",""],
        fTemp: ["","","","",""],
        fCond: ["","","","",""],
        fHum: ["","","","",""],
        fPre: ["","","","",""],
    }
})

fetch(URL)
    .then((resp) => resp.json()) // Transform the data into json
    .then(function (data) {
        //console.log(data);

        var weatherURL = 'https://api.openweathermap.org/data/2.5/weather?zip=' + data.zip + weatherAPI + '&units=imperial';
        var forcastURL = 'http://api.openweathermap.org/data/2.5/forecast?zip=' + data.zip + weatherAPI + '&units=imperial';

        fetch(weatherURL)
            .then((resp) => resp.json()) // Transform the data into json
            .then(function (weatherData) {
                // Create and append the li's to the ul
                console.log(weatherData);
                weather.time = new Date().toLocaleString();
                weather.temperature = weatherData.main.temp;
                weather.conditions = weatherData.weather[0].description;
                weather.humidity = weatherData.main.humidity;
                weather.pressure = weatherData.main.pressure;
            })


        fetch(forcastURL)
            .then((resp) => resp.json()) // Transform the data into json
            .then(function (forcastData) {
                // Create and append the li's to the ul
                console.log(forcastData);

                var i;
                var j = 0;
                for (i=0; i<=4; i++)
                {
                    forecast.date.splice(i, new Date().toLocaleString());
                    forecast.date[i] = forcastData.list[j].dt_txt.slice(5,16);
                    forecast.fTemp[i] = forcastData.list[j].main.temp;
                    forecast.fCond[i] = forcastData.list[j].weather[0].description;
                    forecast.fHum[i] = forcastData.list[j].main.humidity;
                    forecast.fPre[i] = forcastData.list[j].main.pressure;
                    j+=8;
                }
            })
    });

